from django.db import models
from .Categoria import Categoria


class Subcategoria(models.Model):
    id_subcategoria = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=100)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)
    categoria = models.ForeignKey(
        Categoria,
        on_delete=models.CASCADE,
        related_name='subcategorias',
        db_column='id_categoria',
    )

    def __str__(self):
        return self.descripcion

    class Meta:
        verbose_name_plural = 'Subcategorias'
        verbose_name = 'Subcategoria'
        db_table = 'subcategoria'
