from django.db import models


class Categoria(models.Model):
    id_categoria = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=100)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.descripcion

    class Meta:
        verbose_name_plural = 'Categorias'
        verbose_name = 'Categoria'
        db_table = 'categoria'
