from django.contrib import admin
from ..models import Categoria, Subcategoria


class SubcategoriaInline(admin.StackedInline):
    model = Subcategoria
    extra = 2


class CategoriaAdmin(admin.ModelAdmin):
    inlines = [
        SubcategoriaInline
    ]
    list_display = ('descripcion', 'fecha_creacion', 'id_categoria')
    # list_editable = ('descripcion',)
    list_filter = ['fecha_creacion']
    search_fields = ('descripcion',)


admin.site.register(Categoria, CategoriaAdmin)
