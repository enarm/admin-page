from django.contrib import admin
from ..models import Subcategoria


class SubcategoriaAdmin(admin.ModelAdmin):
    list_display = ('descripcion', 'id_subcategoria', 'categoria')
    list_filter = ['fecha_creacion', 'categoria']
    search_fields = ('descripcion',)


admin.site.register(Subcategoria, SubcategoriaAdmin)
