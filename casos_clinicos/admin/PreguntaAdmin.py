from django.contrib import admin
from ..models import Pregunta, Respuesta


class RespuestaInline(admin.StackedInline):
    model = Respuesta
    extra = 1


class PreguntaAdmin(admin.ModelAdmin):
    inlines = [
        RespuestaInline,
    ]
    list_display = ('descripcion', 'id_pregunta', 'caso_clinico')
    list_filter = ['fecha_creacion']
    search_fields = ('descripcion',)
    readonly_fields = ['caso_clinico']

    id_pregunta = None

    def get_form(self, request, obj=None, **kwargs):
        if obj:
            self.id_pregunta = obj.id_pregunta
        return super(PreguntaAdmin, self).get_form(request, obj, **kwargs)

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'respuesta_correcta':
            kwargs["queryset"] = Respuesta.objects.filter(
                pregunta=self.id_pregunta)
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


admin.site.register(Pregunta, PreguntaAdmin)
