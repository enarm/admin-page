from django.contrib import admin
from ..models import Pregunta, CasoClinico


class PreguntaInline(admin.StackedInline):
    model = Pregunta
    extra = 1
    exclude = ('respuesta_correcta', )
    show_change_link = True


class CasoClinicoAdmin(admin.ModelAdmin):
    inlines = [
        PreguntaInline
    ]
    list_display = ('descripcion', 'fecha_creacion', 'id_caso_clinico')
    # list_editable = ('descripcion',)
    list_filter = ['fecha_creacion']
    search_fields = ('descripcion',)


admin.site.register(CasoClinico, CasoClinicoAdmin)
