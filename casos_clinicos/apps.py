from django.apps import AppConfig


class CasosClinicosConfig(AppConfig):
    name = 'casos_clinicos'
    verbose_name = 'Casos Clínicos'
