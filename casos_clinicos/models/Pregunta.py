from django.db import models
from .CasoClinico import CasoClinico


class Pregunta(models.Model):
    id_pregunta = models.AutoField(primary_key=True)
    descripcion = models.TextField()
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)
    caso_clinico = models.ForeignKey(
        CasoClinico,
        on_delete=models.CASCADE,
        related_name='preguntas',
        db_column='id_caso_clinico',
    )
    respuesta_correcta = models.OneToOneField(
        'casos_clinicos.Respuesta',
        on_delete=models.CASCADE,
        related_name='respuesta_correcta',
        db_column='id_respuesta_correcta',
        null=True,
        blank=True
    )

    def __str__(self):
        return self.descripcion

    class Meta:
        verbose_name_plural = "Preguntas"
        verbose_name = 'Pregunta'
        db_table = 'pregunta'
