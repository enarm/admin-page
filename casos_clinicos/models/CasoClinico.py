from django.db import models


class CasoClinico(models.Model):
    id_caso_clinico = models.AutoField(primary_key=True)
    descripcion = models.TextField()
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)
    subcategoria = models.ForeignKey(
        'categorias.Subcategoria',
        on_delete=models.CASCADE,
        related_name='subcategorias',
        db_column='id_subcategoria',
    )

    def __str__(self):
        return self.descripcion

    class Meta:
        verbose_name_plural = "Casos Clínicos"
        verbose_name = 'Caso Clínico'
        db_table = "caso_clinico"
