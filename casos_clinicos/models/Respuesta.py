from django.db import models
from .Pregunta import Pregunta


class Respuesta(models.Model):
    id_respuesta = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=300)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)
    pregunta = models.ForeignKey(
        Pregunta,
        on_delete=models.CASCADE,
        related_name='respuestas',
        db_column='id_pregunta',
        blank=True,
        null=True,
    )

    def __str__(self):
        return self.descripcion

    class Meta:
        verbose_name_plural = "Respuestas"
        verbose_name = 'Respuesta'
        db_table = 'respuesta'
