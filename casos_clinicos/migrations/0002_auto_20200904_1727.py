# Generated by Django 3.1.1 on 2020-09-04 17:27

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('casos_clinicos', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pregunta',
            name='respuesta_correcta',
            field=models.ForeignKey(db_column='id_respuesta_correcta', on_delete=django.db.models.deletion.CASCADE, related_name='respuesta_correcta', to='casos_clinicos.respuesta'),
        ),
    ]
