from rest_framework import serializers
from casos_clinicos.models import CasoClinico


class CasoClinicoSerializer(serializers.ModelSerializer):
    class Meta:
        model = CasoClinico
        fields = ('id_caso_clinico', 'descripcion')
