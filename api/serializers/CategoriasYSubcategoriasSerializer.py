from rest_framework import serializers
from categorias.models import Categoria
from categorias.models import Subcategoria


class SubcategoriaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Subcategoria
        fields = ('id_subcategoria', 'descripcion')


class CategoriasYSubcategoriasSerializer(serializers.ModelSerializer):
    subcategorias = SubcategoriaSerializer(many=True, read_only=True)

    class Meta:
        model = Categoria
        fields = ('id_categoria', 'descripcion', 'subcategorias')
