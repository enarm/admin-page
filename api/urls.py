from django.urls import path
from .views import CasoClinicoAPIView, CategoriasYSubcategoriasAPIView

urlpatterns = [
    path('', CasoClinicoAPIView.as_view()),
    path('categorias-y-subcategorias/', CategoriasYSubcategoriasAPIView.as_view())
]
