from rest_framework import generics
from casos_clinicos.models import CasoClinico
from ..serializers import CasoClinicoSerializer


class CasoClinicoAPIView(generics.ListAPIView):
    queryset = CasoClinico.objects.all()
    serializer_class = CasoClinicoSerializer
