from rest_framework import generics
from categorias.models import Categoria
from ..serializers import CategoriasYSubcategoriasSerializer


class CategoriasYSubcategoriasAPIView(generics.ListAPIView):
    queryset = Categoria.objects.filter()
    serializer_class = CategoriasYSubcategoriasSerializer
