from django.db import models
from django import forms


class Estudiante(models.Model):
    id_estudiante = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=100)
    email = models.EmailField()
    password = models.CharField(max_length=128)
    universidad = models.CharField(max_length=100)
    edad = models.IntegerField()
    especialidad = models.CharField(max_length=100)
    fecha_creacion = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = 'Estudiantes'
        verbose_name = 'Estudiante'
        db_table = 'estudiante'
