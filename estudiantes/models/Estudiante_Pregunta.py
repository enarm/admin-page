from django.db import models
from .Estudiante import Estudiante
from casos_clinicos.models import Pregunta


class Estudiante_Pregunta(models.Model):
    id_estudiante_pregunta = models.AutoField(primary_key=True)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    estudiante = models.ForeignKey(
        Estudiante,
        on_delete=models.CASCADE,
        related_name='estudiantes_preguntas',
        db_column='id_estudiante',
    )
    pregunta = models.ForeignKey(
        Pregunta,
        on_delete=models.CASCADE,
        related_name='estudiantes_preguntas',
        db_column='id_pregunta',
    )
    respuesta_correcta = models.BooleanField(default=False)
    tiempo_respuesta = models.FloatField(default=0)

    class Meta:
        db_table = 'estudiante_pregunta'
