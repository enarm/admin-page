from django.contrib import admin
from ..models import Estudiante


class EstudianteAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'email', 'universidad', 'especialidad')


admin.site.register(Estudiante, EstudianteAdmin)
