from django.contrib import admin
from ..models import Estudiante_Pregunta


class Estudiante_PreguntaAdmin(admin.ModelAdmin):
    list_display = ('pregunta', 'pregunta',
                    'respuesta_correcta', 'tiempo_respuesta')


admin.site.register(Estudiante_Pregunta, Estudiante_PreguntaAdmin)
